#!/bin/bash

# Specify the Istio version that will be leveraged throughout these instructions
ISTIO_VERSION=1.2.5

# MacOS
curl -sL "https://github.com/istio/istio/releases/download/$ISTIO_VERSION/istio-$ISTIO_VERSION-osx.tar.gz" | tar xz

cd istio-$ISTIO_VERSION
sudo cp ./bin/istioctl /usr/local/bin/istioctl
sudo chmod +x /usr/local/bin/istioctl

# Generate the bash completion file and source it in your current shell
mkdir -p ~/completions && istioctl collateral --bash -o ~/completions
source ~/completions/istioctl.bash

# Source the bash completion file in your .bashrc so that the command-line completions
# are permanently available in your shell
echo "source ~/completions/istioctl.bash" >> ~/.bashrc

helm install install/kubernetes/helm/istio-init --name istio-init --namespace istio-system

kubectl get jobs -n istio-system

kubectl get crds | grep 'istio.io' | wc -l


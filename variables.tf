variable "client_id" {
  type    = "string"
  default = "1aa9a1cb-8f03-4303-a396-57e11d0bc466"
}

variable "client_secret" {
  type    = "string"
  default = "N[EAkbRO.BkCi+-z2oJ8ktuluwKZhC17"
}

variable "agent_count" {
  type    = "string"
  default = 3
}

variable "ssh_public_key" {
  type    = "string"
  default = "~/.ssh/id_rsa.pub"
}

variable "dns_prefix" {
  type    = "string"
  default = "k8stest"
}

variable cluster_name {
  type    = "string"
  default = "k8stest"
}

variable resource_group_name {
  type    = "string"
  default = "nic-k8stest"
}

variable location {
  type    = "string"
  default = "Central US"
}
